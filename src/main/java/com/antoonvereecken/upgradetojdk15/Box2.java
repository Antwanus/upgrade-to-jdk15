package com.antoonvereecken.upgradetojdk15;

public class Box2 {
    public static void main(String[] args) {
        Number number = 10;

        if(number instanceof Integer i) { /* auto-casts the number to an integer "i" */ }

    }
}
