package com.antoonvereecken.upgradetojdk15;

public class Box1 {
    public static void main(String[] args) {
        System.out.println("""
                        HELLO          e  
                        PALeeeeeeee                  eeeeeeeeeeeee
                """);
    }

    public void method() {
        enum E { a, b, c }
        class localClass { }
        interface  localInterface{ }
        record e() { }
    }
}
