package com.antoonvereecken.upgradetojdk15;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SandboxJdk15Application {

	public static void main(String[] args) {
		SpringApplication.run(SandboxJdk15Application.class, args);
	}

}
